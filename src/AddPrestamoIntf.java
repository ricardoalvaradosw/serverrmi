
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


/**
 *
 * @author Ricardo
 */
public interface AddPrestamoIntf extends Remote{
    public void relizarPrestamo(Prestamo prestamo, List<Libro> libros) throws RemoteException;
}
