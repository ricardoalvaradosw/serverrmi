
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Ricardo
 */
public class AddMiembroIntImpl extends UnicastRemoteObject implements AddMiembroItf {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String host, port, bd, user, pass;

    public AddMiembroIntImpl(String host, String puerto, String nombre, String user, String pass) throws RemoteException {
        this.host = host;
        this.port = puerto;
        this.bd = nombre;
        this.user = user;
        this.pass = pass;
    }

    @Override
    public boolean save(Miembro miembro) throws RemoteException {
        Boolean retorno = false;
        try {
            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("insert into miembro(nombre,apellido,direccion,telefono,email,dni,sexo,edad,estado)"
                    + "values(?,?,?,?,?,?,?,?,?)");
            ps.setString(1, miembro.getNombre());
            ps.setString(2, miembro.getApellido());
            ps.setString(3, miembro.getDireccion());
            ps.setString(4, miembro.getTelefono());
            ps.setString(5, miembro.getEmail());
            ps.setString(6, miembro.getDni());
            ps.setString(7, miembro.getSexo());
            ps.setInt(8, miembro.getEdad());
            ps.setBoolean(9, true);
            retorno = (ps.executeUpdate() > 0) ? true : false;
            con.commit();
            return retorno;

        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
        return retorno;
    }

    @Override
    public boolean update(Miembro miembro) throws RemoteException {
        Boolean retorno = false;
        try {

            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("Update miembro set nombre = ? , apellido = ? , direccion = ? , telefono = ? , email = ? , dni=? ,sexo=?,edad=? where id_miembro = ? ");
            ps.setString(1, miembro.getNombre());
            ps.setString(2, miembro.getApellido());
            ps.setString(3, miembro.getDireccion());
            ps.setString(4, miembro.getTelefono());
            ps.setString(5, miembro.getEmail());
            ps.setString(6, miembro.getDni());
            ps.setString(7, miembro.getSexo());
            ps.setInt(8, miembro.getEdad());
            ps.setInt(9, miembro.getId_miembro() );
            retorno = (ps.executeUpdate() > 0) ? true : false;
            con.commit();
            return retorno;
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);

        }
        return retorno;
    }

    @Override
    public boolean delete(Miembro miembro) throws RemoteException {
        Boolean retorno = false;
        try {
            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("update miembro set estado=? where id_miembro=? ");
            ps.setBoolean(1, false);
            ps.setInt(2, miembro.getId_miembro());
            retorno = (ps.executeUpdate() > 0) ? true : false;
            con.commit();
            return retorno;
        } catch (Exception e) {
            try {
                if (con != null) {
                    con.rollback();
                }

                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
        return retorno;
    }

    @Override
    public Miembro find(int id) throws RemoteException {
        Miembro miembro = null;
        try {

            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("select * from miembro where id_miembro= ? and estado=?");
            ps.setInt(1, id);
            ps.setBoolean(2, true);
            rs = ps.executeQuery();
            if (rs.next()) {
                miembro = new Miembro();
                miembro.setId_miembro(rs.getInt(1));
                miembro.setNombre(rs.getString(2));
                miembro.setApellido(rs.getString(3));
                miembro.setDireccion(rs.getString(4));
                miembro.setTelefono(rs.getString(5));
                miembro.setEmail(rs.getString(6));
                miembro.setDni(rs.getString(7));
                miembro.setEdad(rs.getInt(9));
                miembro.setSexo(rs.getString(8));
                miembro.setEstado(rs.getBoolean(10));

            }
            con.commit();
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
        return miembro;
    }

    @Override
    public List<Miembro> findAll() throws RemoteException {
        Miembro miembro = null;
        List<Miembro> miembros = new ArrayList<Miembro>();
        try {

            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("select * from miembro where estado=?");
            ps.setBoolean(1, true);
            rs = ps.executeQuery();
            while (rs.next()) {
                miembro = new Miembro();
                miembro.setId_miembro(rs.getInt(1));
                miembro.setNombre(rs.getString(2));
                miembro.setApellido(rs.getString(3));
                miembro.setDireccion(rs.getString(4));
                miembro.setTelefono(rs.getString(5));
                miembro.setEmail(rs.getString(6));
                miembro.setDni(rs.getString(7));
                miembro.setEdad(rs.getInt(9));
                miembro.setSexo(rs.getString(8));
                miembro.setEstado(rs.getBoolean(10));
                miembros.add(miembro);
            }
            con.commit();
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
        return miembros;
    }

    @Override
    public List<Miembro> findByQuery(Miembro m) throws RemoteException {

        List<Miembro> miembros = new ArrayList<Miembro>();
        Miembro miembro = null;
        String query = "";
        try {

            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            if (m.getId_miembro() != 0) {
                ps = con.prepareStatement("select * from miembro where id_miembro=? and nombre like ? and dni like ? and estado=?");
                ps.setInt(1, m.getId_miembro());
                ps.setString(2, "%" + m.getNombre() + "%");
                ps.setString(3,"%"+ m.getDni()+"%");
                ps.setBoolean(4, true);
            } else {
                ps = con.prepareStatement("select * from miembro where nombre like ? and dni like ? and estado=?");
                ps.setString(1, "%" + m.getNombre() + "%");
                ps.setString(2, "%"+ m.getDni()+"%");
                ps.setBoolean(3, true);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                miembro = new Miembro();
                miembro.setId_miembro(rs.getInt(1));
                miembro.setNombre(rs.getString(2));
                miembro.setApellido(rs.getString(3));
                miembro.setDireccion(rs.getString(4));
                miembro.setTelefono(rs.getString(5));
                miembro.setEmail(rs.getString(6));
                miembro.setDni(rs.getString(7));
                miembro.setEdad(rs.getInt(9));
                miembro.setSexo(rs.getString(8));
                miembro.setEstado(rs.getBoolean(10));
                miembros.add(miembro);
            }
            con.commit();
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
        return miembros;
    }

    @Override
    public String holaNoshma() throws RemoteException {
        return "Hola Noshma";
    }
}
