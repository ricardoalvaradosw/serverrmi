
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
/**
 *
 * @author Ricardo
 */
public class Util {

    private static String JDBC_URL;
    private static String JDBC_PORT_BD, JDBC_BD, JDBC_HOST_BD, JDBC_PUERTO_SERVICIO;
    private static String JDBC_DRIVER;
    private static String JDBC_USER;
    private static String JDBC_PASS;
    private static String JDBC_FILE_NAME = "configuracion";
    public static Driver driver = null;

    public static void loadAllProperties(String file) {
        Properties prop = new Properties();
        ResourceBundle bundle = ResourceBundle.getBundle(file);
        Enumeration e = bundle.getKeys();
        String key = null;
        while (e.hasMoreElements()) {
            key = (String) e.nextElement();
            prop.put(key, bundle.getObject(key));
        }
        JDBC_DRIVER = prop.getProperty("driver");
        JDBC_USER = prop.getProperty("usuario");
        JDBC_PASS = prop.getProperty("contrasena");
        JDBC_HOST_BD = prop.getProperty("hostbd");
        JDBC_PORT_BD = prop.getProperty("puertobd");
        JDBC_BD = prop.getProperty("bd");
        JDBC_URL = prop.getProperty("url");
        JDBC_URL = JDBC_URL + JDBC_HOST_BD + ":" + JDBC_PORT_BD + "/" + JDBC_BD;
        JDBC_PUERTO_SERVICIO = prop.getProperty("puertoservicio");
    }

    public static Properties loadPropeties(String file) {
        Properties prop = new Properties();
        ResourceBundle bundle = ResourceBundle.getBundle(file);
        Enumeration e = bundle.getKeys();
        String key = null;
        while (e.hasMoreElements()) {
            key = (String) e.nextElement();
            prop.put(key, bundle.getObject(key));
        }

        JDBC_DRIVER = prop.getProperty("driver");
        JDBC_USER = prop.getProperty("usuario");
        JDBC_PASS = prop.getProperty("contrasena");
        JDBC_HOST_BD = prop.getProperty("hostbd");
        JDBC_PORT_BD = prop.getProperty("puertobd");
        JDBC_BD = prop.getProperty("bd");
        JDBC_URL = prop.getProperty("url");
        JDBC_URL = JDBC_URL + JDBC_HOST_BD + ":" + JDBC_PORT_BD + "/" + JDBC_BD;
        return prop;
    }

    public static synchronized Connection Conectar(String host, String puerto, String nombre, String user, String pass) throws SQLException {
        try {
            Class classJDBCDriver = Class.forName("com.mysql.jdbc.Driver");
            driver = (Driver) classJDBCDriver.newInstance();
            DriverManager.registerDriver(driver);
            String url = "jdbc:mysql://" + host + ":" + puerto + "/" + nombre;
            return DriverManager.getConnection(url, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static synchronized Connection Conectar() throws SQLException {

        try {
            loadPropeties(JDBC_FILE_NAME);
            Class classJDBCDriver = Class.forName(JDBC_DRIVER);
            driver = (Driver) classJDBCDriver.newInstance();
            DriverManager.registerDriver(driver);
            return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void closeConnection(Connection con) {
        try {
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closePS(PreparedStatement ps) {
        try {
            if (ps != null) {
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Map<String, String> leerArchivo(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        Map<String, String> parametros = new HashMap<String, String>();
        String[] cadena = null;
        try {
            archivo = new File(nombreArchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null) {
                cadena = new String[2];
                cadena = linea.split(":");
                parametros.put(cadena[0], cadena[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return parametros;
    }

    public static void guardarConfiguracionBDEnArchivo(String nombreArchivo, Map<String, String> parametros) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            borrarContenidoArchivo(nombreArchivo);
            fichero = new FileWriter(nombreArchivo);
            pw = new PrintWriter(fichero);
            pw.println("hostbd:" + parametros.get("hostbd"));
            pw.println("puertobd:" + parametros.get("puertobd"));
            pw.println("nombre:" + parametros.get("nombre"));
            pw.println("usuario:" + parametros.get("usuario"));
            pw.println("password:" + parametros.get("password"));
            pw.println("hostservidor:" + parametros.get("hostservidor"));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void borrarContenidoArchivo(String nombreArchivo) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(nombreArchivo));
            bw.write("");
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
