
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ricardo
 */
public class AddPrestamoIntfImpl extends UnicastRemoteObject implements AddPrestamoIntf {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String host, port, bd, user, pass;

    public AddPrestamoIntfImpl(String host, String puerto, String nombre, String user, String pass) throws RemoteException {
        this.host = host;
        this.port = puerto;
        this.bd = nombre;
        this.user = user;
        this.pass = pass;
    }

    @Override
    public void relizarPrestamo(Prestamo prestamo, List<Libro> libros) throws RemoteException {
       PreparedStatement ps1,ps2,ps3,ps4;
       ResultSet rs1;
       int idprestamo = 0;
       try {
            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
            con.setAutoCommit(false);
            ps1 = con.prepareStatement("insert into prestamo (id_miembro,fecha_prestamo,fecha_devolucion,observaciones) values (?,?,?,?) ");
            ps1.setInt(1, prestamo.getId_miembro());
            ps1.setDate(2, new Date(prestamo.getFecha_prestamo().getTime()));
            ps1.setDate(3, new Date(prestamo.getFecha_devolucion().getTime()));
            ps1.setString(4, prestamo.getObservaciones());
            ps1.execute();
            ps4 = con.prepareStatement("select last_insert_id() as last_id from prestamo",Statement.RETURN_GENERATED_KEYS);
          
            rs1 = ps4.executeQuery();
            if(rs1.next()){
                idprestamo = rs1.getInt(1);
            }
            for(Libro libro : libros){
                ps2 = con.prepareStatement("insert into detalle_prestamo (id_libro,id_prestamo) values (?,?)");
                ps2.setInt(1, libro.getId_libro());
                ps2.setInt(2, idprestamo);
                ps2.execute();
                ps3 = con.prepareStatement("update libro set stock=stock-1 where id_libro = ?");
                ps3.setInt(1, libro.getId_libro());
                ps3.execute();
            }
            con.commit();

        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
    }
}
