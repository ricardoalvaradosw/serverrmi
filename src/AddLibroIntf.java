
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
/**
 *
 * @author Ricardo
 */
public interface AddLibroIntf extends Remote{
    public void prestarLibro(Libro libro) throws RemoteException;
    public void CancelarPrestamo(List<Libro> libros) throws RemoteException;
    public List<Libro> findAllByQuery(Libro libro)throws RemoteException;
    public List<Libro> findAll()throws RemoteException;
    public Libro find(int id) throws RemoteException;
}
