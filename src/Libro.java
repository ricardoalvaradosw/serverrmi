
import java.io.Serializable;

/**
 *
 * @author Ricardo
 */
public class Libro implements Serializable{
    private int id_libro;
    private String isbn;
    private String nombre;
    private String autor;
    private String genero;
    private String editorial;
    private int numpag;
    private String añoedicion;
    private int stock;
    private String estado;

    public Libro() {
    }
    
    

    public Libro(int id_libro, String isbn, String nombre, String autor, String genero, String editorial, int numpag, int stock, String estado) {
        this.id_libro = id_libro;
        this.isbn = isbn;
        this.nombre = nombre;
        this.autor = autor;
        this.genero = genero;
        this.editorial = editorial;
        this.numpag = numpag;
        this.stock = stock;
        this.estado = estado;
    }
    
    

    public int getId_libro() {
        return id_libro;
    }

    public void setId_libro(int id_libro) {
        this.id_libro = id_libro;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public int getNumpag() {
        return numpag;
    }

    public void setNumpag(int numpag) {
        this.numpag = numpag;
    }

    public String getAñoedicion() {
        return añoedicion;
    }

    public void setAñoedicion(String añoedicion) {
        this.añoedicion = añoedicion;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Libro{" + "id_libro=" + id_libro + ", isbn=" + isbn + ", nombre=" + nombre + ", autor=" + autor + ", genero=" + genero + ", editorial=" + editorial + ", numpag=" + numpag + ", a\u00f1oedicion=" + añoedicion + ", stock=" + stock + ", estado=" + estado + '}';
    }
    
    
    
}
