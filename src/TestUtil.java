
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ricardo
 */
public class TestUtil {

    public static void main(String[] args) throws SQLException, UnknownHostException {
//        Map<String,String> parametros = Util.leerArchivo("parametros.txt");
//        System.out.println("Host " + parametros.get("hostbd"));
//        System.out.println("puerto " + parametros.get("puertobd"));
//        System.out.println("nombre " + parametros.get("nombre"));
//        System.out.println("user " + parametros.get("usuario"));
//        System.out.println("pass " + parametros.get("password"));
        System.out.println(Inet4Address.getLocalHost().getHostAddress());
        try {
            AddMiembroItf m = new AddMiembroIntImpl("localhost", "3306", "biblioteca", "root", "region");
            AddLibroIntf l = new AddLibroIntImpl("localhost", "3306", "biblioteca", "root", "region");
            Libro li = l.find(2);
            System.out.println("li" + li.getNombre());
             List<Miembro> miembros = m.findAll();
             for(Miembro mi : miembros){
                 System.out.println("mi " + mi.getApellido());
             }
        } catch (RemoteException ex) {
            Logger.getLogger(TestUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
}
