
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author Ricardo
 */
public interface AddMiembroItf extends Remote{
    public boolean save(Miembro miembro) throws RemoteException;
    public boolean update(Miembro miembro) throws RemoteException;
    public boolean delete(Miembro miembro) throws RemoteException;
    public Miembro find(int id) throws RemoteException;
    public List<Miembro> findAll() throws RemoteException;
    public List<Miembro> findByQuery(Miembro miembro) throws RemoteException;
    public String holaNoshma() throws RemoteException;
    
}
