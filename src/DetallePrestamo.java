
import java.io.Serializable;

/**
 *
 * @author Ricardo
 */
public class DetallePrestamo implements Serializable{
    private int id_libro;
    private int id_prestamo;

    public DetallePrestamo() {
    }

    public DetallePrestamo(int id_libro, int id_prestamo) {
        this.id_libro = id_libro;
        this.id_prestamo = id_prestamo;
    }
    
    public int getId_libro() {
        return id_libro;
    }

    public void setId_libro(int id_libro) {
        this.id_libro = id_libro;
    }

    public int getId_prestamo() {
        return id_prestamo;
    }

    public void setId_prestamo(int id_prestamo) {
        this.id_prestamo = id_prestamo;
    }

    @Override
    public String toString() {
        return "DetallePrestamo{" + "id_libro=" + id_libro + ", id_prestamo=" + id_prestamo + '}';
    }
    
    
}
