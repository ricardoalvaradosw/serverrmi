
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Ricardo
 */

public class AddLibroIntImpl extends UnicastRemoteObject implements AddLibroIntf {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String host, port, bd, user, pass;

    public AddLibroIntImpl(String host, String puerto, String nombre, String user, String pass) throws RemoteException {
        super();
        this.host = host;
        this.port = puerto;
        this.bd = nombre;
        this.user = user;
        this.pass = pass;
    }

    @Override
    public void prestarLibro(Libro libro) throws RemoteException {
        try {
            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("update libro set stock = stock-1 where id_libro = ? ");
            ps.setInt(1, libro.getId_libro());
            ps.executeUpdate();
            con.commit();
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
    }

    @Override
    public void CancelarPrestamo(List<Libro> libros) throws RemoteException {
        try {
            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            for (Libro l : libros) {
                ps = con.prepareStatement("update libro set stock = stock+1 where id_libro = ? ");
                ps.setInt(1, l.getId_libro());
                ps.executeUpdate();
            }


            con.commit();
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
    }

    @Override
    public List<Libro> findAllByQuery(Libro l) throws RemoteException {
        List<Libro> libros = new ArrayList<Libro>();
        Libro libro = null;
        try {
            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("select * from libro where  stock > ?  and nombre like ? and autor like ? and genero like ?");
     
            ps.setInt(1, 0);
            ps.setString(2, "%" + l.getNombre() + "%");
            ps.setString(3, "%" + l.getAutor() + "%");
            ps.setString(4, "%" + l.getGenero() + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                libro = new Libro();
                libro.setId_libro(rs.getInt(1));
                libro.setIsbn(rs.getString(2));
                libro.setNombre(rs.getString(3));
                libro.setAutor(rs.getString(4));
                libro.setGenero(rs.getString(5));
                libro.setEditorial(rs.getString(6));
                libro.setNumpag(rs.getInt(7));
                libro.setAñoedicion(rs.getString(8));
                libro.setStock(rs.getInt(9));
                libros.add(libro);
            }
            con.commit();
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
        return libros;
    }

    @Override
    public List<Libro> findAll() throws RemoteException {
        List<Libro> libros = new ArrayList<Libro>();
        Libro libro = null;
        try {
            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("select * from libro where stock > ?");
            ps.setInt(1, 0);
            rs = ps.executeQuery();
            while (rs.next()) {
                libro = new Libro();
                libro.setId_libro(rs.getInt(1));
                libro.setIsbn(rs.getString(2));
                libro.setNombre(rs.getString(3));
                libro.setAutor(rs.getString(4));
                libro.setGenero(rs.getString(5));
                libro.setEditorial(rs.getString(6));
                libro.setNumpag(rs.getInt(7));
                libro.setAñoedicion(rs.getString(8));
                libro.setStock(rs.getInt(9));
           
                libros.add(libro);
            }
            con.commit();
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
        return libros;
    }

    @Override
    public Libro find(int id) throws RemoteException {
          Libro libro = null;
        try {
            con = Util.Conectar(this.host, this.port, this.bd, this.user, this.pass);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            con.setAutoCommit(false);
            ps = con.prepareStatement("select * from libro where id_libro =?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                libro = new Libro();
                libro.setId_libro(rs.getInt(1));
                libro.setIsbn(rs.getString(2));
                libro.setNombre(rs.getString(3));
                libro.setAutor(rs.getString(4));
                libro.setGenero(rs.getString(5));
                libro.setEditorial(rs.getString(6));
                libro.setNumpag(rs.getInt(7));
                libro.setAñoedicion(rs.getString(8));
                libro.setStock(rs.getInt(9));
           
            }
            con.commit();
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
            } catch (SQLException ex) {
                Logger.getLogger(AddMiembroIntImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            Util.closePS(ps);
            Util.closeConnection(con);
        }
        return libro;
    }
}
